import logging

from glottolog_service import settings
from glottolog_service.settings import LOG_FILE_HANDLER, LOG_FILENAME

bind = '0.0.0.0:8001'
workers = 4
timeout = 180
reload = True
raw_env = [
    'DJANGO_SETTINGS_MODULE=glottolog_service.settings'
]
loglevel = 'debug' if settings.DEBUG else 'info'
capture_output = True
log_file = LOG_FILENAME
logfile = log_file
errorlog = log_file
accesslog = log_file
access_log_format = '%(h)s %(l)s %(u)s %(t)s "%(r)s" %(s)s %(b)s "%(f)s" "%(a)s"'

LOGGER = logging.getLogger(__name__)
LOGGER.addHandler(LOG_FILE_HANDLER)
