#!/bin/sh
echo "Exporting GraphQL Schema"
python manage.py graphql_schema --schema glottolog.graphql.schema.schema --out glottolog/static/graphql/schema.graphql

echo "COLLECTING STATIC FILES"
python manage.py collectstatic --no-input

gunicorn glottolog_service.wsgi:application -c deployment/gunicorn_config.py