from rest_framework import serializers

from glottolog import models


class InfoSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Info
        fields = '__all__'


class AESSourceSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.AESSource
        fields = '__all__'


class AESStatusSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.AESStatus
        fields = '__all__'


class EndangermentSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Endangerment
        fields = '__all__'

    source = AESSourceSerializer()
    status = AESStatusSerializer()


class CountrySerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Country
        fields = '__all__'


class MacroareaSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Macroarea
        fields = '__all__'


class PrunedLanguoidSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Languoid
        fields = ('glottocode', 'name')


class LanguoidSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Languoid
        fields = '__all__'

    endangerment = EndangermentSerializer()
    ancestors = PrunedLanguoidSerializer(many=True)
    descendants = PrunedLanguoidSerializer(many=True)
    parent = PrunedLanguoidSerializer()
    children = PrunedLanguoidSerializer(many=True)
    countries = CountrySerializer(many=True)
    macroareas = MacroareaSerializer(many=True)
    family = PrunedLanguoidSerializer()
