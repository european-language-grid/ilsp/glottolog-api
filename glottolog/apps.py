from django.apps import AppConfig


class GlottologConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'glottolog'
