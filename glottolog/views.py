# Create your views here.
from rest_framework import viewsets
from rest_framework.decorators import api_view
from rest_framework.pagination import LimitOffsetPagination
from rest_framework.response import Response

from glottolog.mixins import MultipleFieldLookupMixin
from glottolog.models import Info, Languoid
from glottolog.serializers import InfoSerializer, LanguoidSerializer


@api_view(http_method_names=['GET'])
def index(request):
    info = Info.objects.first()
    response = InfoSerializer(info).data
    del response['id']
    del response['publisher']['contact']
    return Response(response)


class LanguoidViewSet(MultipleFieldLookupMixin, viewsets.ReadOnlyModelViewSet):
    """ViewSet for the MetadataRecord class"""

    pagination_class = LimitOffsetPagination
    queryset = Languoid.objects.all()
    serializer_class = LanguoidSerializer
    lookup_fields = ('glottocode', 'iso_code', 'pk')
