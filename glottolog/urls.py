from django.urls import path, include
from rest_framework import routers

from glottolog import views

router = routers.DefaultRouter()
router.register('languoid', views.LanguoidViewSet, basename='languoid')

urlpatterns = (
    path('', include(router.urls)),
)

app_name = 'glottolog'
