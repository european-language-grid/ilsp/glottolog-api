import pycountry
from django.db import models
from django.db.models import JSONField


class Info(models.Model):
    version = models.CharField(max_length=10)
    description = models.CharField(max_length=500, null=True, blank=True)
    domain = models.CharField(max_length=100, null=True, blank=True)
    title = models.CharField(max_length=100, null=True, blank=True)
    url = models.URLField(null=True, blank=True)
    license = JSONField(null=True, blank=True)
    publisher = JSONField(null=True, blank=True)


class Languoid(models.Model):
    ancestors = models.ManyToManyField('Languoid', related_name='descendants')
    category = models.CharField(max_length=100, null=True, blank=True)
    level = models.CharField(max_length=10, null=True, blank=True)
    countries = models.ManyToManyField('Country', related_name='languoids')
    endangerment = models.ForeignKey('Endangerment', on_delete=models.PROTECT, related_name='languoids', null=True)
    family = models.ForeignKey('Languoid', on_delete=models.PROTECT, related_name='languoids', null=True)
    glottocode = models.CharField(max_length=10, unique=True)
    hid = models.CharField(max_length=10, null=True, blank=True)
    iso = models.CharField(max_length=10, null=True, blank=True)
    iso_code = models.CharField(max_length=10, null=True, blank=True)
    isolate = models.BooleanField(null=True)
    longitude = models.FloatField(null=True, blank=True)
    latitude = models.FloatField(null=True, blank=True)
    macroareas = models.ManyToManyField('Macroarea', related_name='languoids')
    name = models.CharField(max_length=100)
    alternative_names = JSONField(null=True, blank=True)
    parent = models.ForeignKey('Languoid', null=True, blank=True, on_delete=models.PROTECT,
                               related_name='children')

    def __str__(self):
        return f'{self.name} - {self.glottocode}'

    def get_geojson(self):
        return dict(
            type='Feature',
            geometry=dict(
                type='Point',
                coordinates=[self.longitude, self.latitude]
            ),
            properties={}
        )


class Country(models.Model):
    identifier = models.CharField(max_length=10, unique=True)
    name = models.CharField(max_length=100)

    def __str__(self):
        return f'{self.name} - {self.identifier}'

    def from_pycountry(self):
        return pycountry.countries.get(alpha_2=self.identifier)


class AESSource(models.Model):
    identifier = models.CharField(max_length=100, unique=True)
    name = models.CharField(max_length=100, null=True, blank=True)
    reference_id = models.CharField(max_length=200, null=True, blank=True)
    url = models.URLField(null=True, blank=True)


class AESStatus(models.Model):
    identifier = models.CharField(max_length=100, unique=True)
    egids = models.CharField(max_length=100)
    elcat = models.CharField(max_length=100)
    icon = models.CharField(max_length=100)
    name = models.CharField(max_length=100)
    ordinal = models.IntegerField(null=True)
    reference_id = models.CharField(max_length=100)
    unesco = models.CharField(max_length=100)


class Endangerment(models.Model):
    date = models.DateTimeField()
    comment = models.CharField(max_length=500, null=True, blank=True)
    source = models.ForeignKey('AESSource', on_delete=models.PROTECT, related_name='%(class)s_related')
    status = models.ForeignKey('AESStatus', on_delete=models.PROTECT, related_name='%(class)s_related')


class Macroarea(models.Model):
    identifier = models.CharField(max_length=100, unique=True)
    name = models.CharField(max_length=100)
    description = models.CharField(max_length=500, null=True, blank=True)
    reference_id = models.CharField(max_length=100)
