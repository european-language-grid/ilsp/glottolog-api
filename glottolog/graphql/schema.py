import graphene
from graphene.types.generic import GenericScalar
from graphene_django import DjangoObjectType
from language_tags import tags
from pycountry import languages

from glottolog.models import Languoid, Endangerment, AESStatus, AESSource, Macroarea, Country
from glottolog.utils import remove_from_list


class Category(graphene.Enum):
    family = 'Family'
    spoken = 'Spoken L1 Language'
    artificial = 'Artificial Language'
    bookkeeping = 'Bookkeeping'
    dialect = 'Dialect'
    mixed = 'Mixed Language'
    pidgin = 'Pidgin'
    pseudo = 'Pseudo Family'
    sign = 'Sign Language'
    speech = 'Speech Register'
    unattested = 'Unattested'
    unclassifiable = 'Unclassifiable'


class LanguoidType(DjangoObjectType):
    class Meta:
        model = Languoid
        fields = '__all__'

    alternative_names: dict = GenericScalar()
    alternative_names_pruned: list = graphene.List(graphene.String)
    bcp47: str = graphene.String()
    geo_json: dict = GenericScalar()

    def resolve_alternative_names_pruned(self, info):
        result = list()
        if self.alternative_names.get('wals'):
            result.extend(self.alternative_names.get('wals'))
        if self.alternative_names.get('lexvo'):
            result.extend(
                [lv.split('[')[0].strip() for lv in self.alternative_names.get('lexvo') if lv.endswith('[en]')])
        if self.name in result:
            remove_from_list(result, self.name)
        return result

    def resolve_bcp47(self, info):
        try:
            lang1 = languages.get(alpha_3=self.iso.lower())
            try:
                lang = tags.language(lang1.alpha_2)
            except AttributeError:
                lang = tags.language(lang1.alpha_3)
            return lang.data.get('subtag')
        except:
            return None

    def resolve_geo_json(self, info):
        return self.get_geojson()


class EndangermentType(DjangoObjectType):
    class Meta:
        model = Endangerment
        fields = '__all__'


class AESStatusType(DjangoObjectType):
    class Meta:
        model = AESStatus
        fields = '__all__'


class AESSourceType(DjangoObjectType):
    class Meta:
        model = AESSource
        fields = '__all__'


class MacroareaType(DjangoObjectType):
    class Meta:
        model = Macroarea
        fields = '__all__'


class CountryType(DjangoObjectType):
    class Meta:
        model = Country
        fields = '__all__'

    pycountry_info: dict = GenericScalar()

    def resolve_pycountry_info(self, info):
        return self.from_pycountry().__dict__


class Query(graphene.ObjectType):
    languoids = graphene.List(LanguoidType, category=graphene.String(required=False, default_value=""),
                              level=graphene.String(required=False, default_value=""))
    languoid = graphene.Field(LanguoidType, glottocode=graphene.String(required=False, default_value=""),
                              iso=graphene.String(required=False, default_value=""))
    check = graphene.Boolean(glottocode=graphene.String(required=True))
    countries = graphene.List(CountryType)
    country = graphene.Field(CountryType, country_code=graphene.String(required=True))

    def resolve_languoids(self, info, category, level):
        if category:
            return Languoid.objects.filter(category=getattr(Category, category).value)
        elif level:
            return Languoid.objects.filter(level=level)
        return Languoid.objects.all().order_by('-name')

    def resolve_languoid(self, info, glottocode, iso):
        try:
            return Languoid.objects.get(glottocode=glottocode)
        except Languoid.DoesNotExist:
            try:
                return Languoid.objects.get(iso=iso.lower())
            except Languoid.DoesNotExist:
                try:
                    lang = languages.get(alpha_2=iso.lower())
                    return Languoid.objects.get(iso=lang.alpha_3)
                except (Languoid.DoesNotExist, AttributeError):
                    return None

    def resolve_check(self, info, glottocode):
        if Languoid.objects.filter(glottocode=glottocode).exists():
            return True
        return False

    def resolve_countries(self, info):
        return Country.objects.all()

    def resolve_country(self, info, country_code):
        try:
            return Country.objects.get(identifier=country_code.upper())
        except Country.DoesNotExist:
            return None


schema = graphene.Schema(query=Query)
