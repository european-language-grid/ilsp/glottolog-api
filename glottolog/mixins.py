from django.http import Http404
from rest_framework.generics import get_object_or_404


class MultipleFieldLookupMixin(object):
    def get_object(self):
        queryset = self.get_queryset()  # Get the base queryset
        queryset = self.filter_queryset(queryset)  # Apply any filter backends
        for field in self.lookup_fields:
            filter = {}
            filter[field] = self.kwargs['pk']
            try:
                return get_object_or_404(queryset, **filter)
            except Http404:
                continue
        raise Http404
