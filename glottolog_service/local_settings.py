import os

from django.core.management.utils import get_random_secret_key
ALLOWED_HOSTS = [os.environ.get('allowed-hosts'), '127.0.0.1']
SECRET_KEY = get_random_secret_key()
DEBUG = False
