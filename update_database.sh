create_db() {
  git checkout tags/"$(git describe --tags "$(git rev-list --tags --max-count=1)")" && echo Current registry tag now set to "$(git describe --tags --abbrev=0)"
  cd ..
  rm -f glottolog-db.sqlite3
  rm glottolog/migrations/[!__init__]*
  python manage.py makemigrations
  python manage.py migrate
  python manage.py runscript build_db --script-args "$LATEST_TAG"
}

cd registry || exit
git clone https://github.com/glottolog/glottolog.git .
git checkout master
git pull origin master
CURRENT_TAG=$(git describe --tags --abbrev=0)
LATEST_TAG=$(git describe --tags "$(git rev-list --tags --max-count=1)")
date +%d/%m/%Y\ %H:%M:%S

if [[ $* == *--force* ]]; then
  create_db
else
  echo "Current registry tag: $CURRENT_TAG"
  if [ "$CURRENT_TAG" = "$LATEST_TAG" ]; then
    echo "Database up to date."
  else
    echo "NEW TAG $LATEST_TAG is found. Checking out tag $LATEST_TAG"
    create_db
  fi
fi
