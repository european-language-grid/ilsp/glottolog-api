import logging
import time
from datetime import timedelta

from django.utils.timezone import make_aware
from pyglottolog import Glottolog

from glottolog import models

GLOTTOLOG_REGISTRY = "registry"
GLOTTOLOG = Glottolog(GLOTTOLOG_REGISTRY, cache=True)
LOGGER = logging.getLogger(__name__)


def add_countries(gl_countries):
    for country in gl_countries:
        models.Country.objects.create(identifier=country.id, name=country.name)


def add_aes_source(gl_aes_source):
    for key, value in list(gl_aes_source.items()):
        models.AESSource.objects.create(identifier=value.id, name=value.name, reference_id=value.reference_id,
                                        url=value.url)


def add_aes_status(gl_aes_status):
    for key, value in list(gl_aes_status.items()):
        models.AESStatus.objects.create(identifier=value.id, egids=value.egids, elcat=value.elcat, icon=value.icon,
                                        name=value.name,
                                        reference_id=value.reference_id, unesco=value.unesco)


def add_macroareas(gl_macroareas):
    for key, value in list(gl_macroareas.items()):
        models.Macroarea.objects.create(identifier=value.id, name=value.name, description=value.description,
                                        reference_id=value.reference_id)


def add_languoid(languoid):
    working_languoid, created = models.Languoid.objects.get_or_create(
        category=languoid.category,
        level=languoid.level.id,
        glottocode=languoid.glottocode,
        hid=languoid.hid,
        iso=languoid.iso,
        iso_code=languoid.iso_code,
        isolate=languoid.isolate,
        longitude=languoid.longitude,
        latitude=languoid.latitude,
        name=languoid.name,
        alternative_names=languoid.names
    )
    if created:
        ancestors = list()
        for ancestor in languoid.ancestors:
            try:
                languoid_obj = models.Languoid.objects.get(glottocode=ancestor.glottocode)
            except models.Languoid.DoesNotExist:
                languoid_obj = add_languoid(ancestor)
            ancestors.append(languoid_obj)
        if ancestors:
            working_languoid.ancestors.set(ancestors)

        if languoid.family:
            try:
                working_languoid.family = models.Languoid.objects.get(glottocode=languoid.family.glottocode)
            except models.Languoid.DoesNotExist:
                working_languoid.family = add_languoid(languoid.family)

        if languoid.parent:
            try:
                working_languoid.parent = models.Languoid.objects.get(glottocode=languoid.parent.glottocode)
            except models.Languoid.DoesNotExist:
                working_languoid.parent = add_languoid(languoid.parent)

        if languoid.endangerment:
            endangerment = models.Endangerment(date=make_aware(languoid.endangerment.date),
                                               comment=languoid.endangerment.comment)
            try:
                source = models.AESSource.objects.get(identifier=languoid.endangerment.source.id)
            except models.AESSource.DoesNotExist:
                source = models.AESSource.objects.create(
                    identifier=languoid.endangerment.source.id,
                    name=languoid.endangerment.source.name,
                    reference_id=languoid.endangerment.source.reference_id,
                    url=languoid.endangerment.source.url
                )

            endangerment.source = source
            endangerment.status = models.AESStatus.objects.get(identifier=languoid.endangerment.status.id)
            endangerment.save()
            working_languoid.endangerment = endangerment

        country_objs = list(
            models.Country.objects.filter(identifier__in=[country.id for country in languoid.countries]))
        if country_objs:
            working_languoid.countries.set(country_objs)
        macroarea_objs = list(
            models.Macroarea.objects.filter(identifier__in=[macroarea.id for macroarea in languoid.macroareas]))
        if macroarea_objs:
            working_languoid.macroareas.set(macroarea_objs)
        try:
            working_languoid.save()
        except:
            print(languoid.id)
    return working_languoid


def add_languoids(languoids):
    for languoid in languoids:
        add_languoid(languoid)


def run(*args):
    start_time = time.time()
    metadata = GLOTTOLOG.dataset_metadata
    version = args[0].split("v")[1]
    LOGGER.info(f'Creating Glottolog Database v{version}...')
    models.Info.objects.create(
        version=version,
        description=metadata.description,
        domain=metadata.domain,
        title=metadata.title,
        url=metadata.url,
        license=metadata.license.__dict__,
        publisher=metadata.publisher.__dict__
    )

    countries = GLOTTOLOG.countries
    LOGGER.info(f'Creating Countries ({len(countries)})...')
    add_countries(countries)

    sources = GLOTTOLOG.aes_sources
    LOGGER.info(f'Creating AES Sources ({len(sources)})...')
    add_aes_source(sources)

    status = GLOTTOLOG.aes_status
    LOGGER.info(f'Creating AES Status ({len(status)})...')
    add_aes_status(status)

    macroareas = GLOTTOLOG.macroareas
    LOGGER.info(f'Creating Macroareas ({len(macroareas)})...')
    add_macroareas(macroareas)

    languoids = list(GLOTTOLOG.languoids())
    LOGGER.info(f'Creating Languoids ({len(languoids)})...')
    add_languoids(languoids)

    LOGGER.info(f'DB build completed. Time elapsed: {str(timedelta(seconds=time.time() - start_time)).split(".")[0]}')
