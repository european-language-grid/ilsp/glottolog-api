FROM python:alpine3.15

RUN apk update \
    && apk add --virtual build-deps gcc musl-dev \
    && apk add --no-cache bash git openssh \
    && apk add wget

ADD ./requirements.txt /requirements.txt
RUN pip install -r requirements.txt

RUN mkdir -p /glottolog_service
ADD . /glottolog_service
WORKDIR /glottolog_service
ENTRYPOINT ./deployment/deploy.sh